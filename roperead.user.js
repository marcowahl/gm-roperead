// ==UserScript==
// @name     roperead
// @description Button to switch to roperead page with all text.
// @updateURL https://openuserjs.org/meta/marcowahl/roperead.meta.js
// @downloadURL https://openuserjs.org/install/marcowahl/roperead.user.js
// @copyright 2020, marcowahl (https://openuserjs.org/users/marcowahl)
// @version  0.0
// @grant    none
// @author   Marco Wahl
// @namespace https://gitlab.com/marcowahl/gm-roperead
// @licence GPL-3.0-or-later; http://www.gnu.org/licenses/gpl-3.0.txt
// @homepageURL https://gitlab.com/marcowahl
// ==/UserScript==

var input = document.createElement("input")
input.type="button"
input.value="⇌"
input.addEventListener("click", moveToRopeRead)
input.setAttribute("style", "font-size:10px;position:absolute;top:0px;left:0px;")
input.style.zIndex = 99999
document.body.appendChild(input)

// go to rope-read page with the text of the browser window.
function moveToRopeRead()
{
    const text = allText()
    window.location = "https://marcowahl.gitlab.io/rope-read#t=" + encodeURIComponent(text)
}

function allText()
{
    var mode = document.designMode
    document.designMode = 'on'
    document.execCommand("selectAll")
    var selObj = window.getSelection()
    document.designMode = mode
    return selObj
}

// {{{ changelog :

// [2020-10-28 Wed] 0.0 initial release.

// }}}

// {{{ contact :

// Request a pull at https://gitlab.com/marcowahl/gm-roperead.

// }}}
